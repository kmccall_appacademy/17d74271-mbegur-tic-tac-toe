class HumanPlayer
  attr_reader :name
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where you would like to move"
    move = gets.chomp.delete(" ,").chars.map(&:to_i)
  end

  def display(board)
    p puts board.grid
  end

end
