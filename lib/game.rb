require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player

  def initialize(player_one, player_two)
    @player_one, @player_two = player_one, player_two
    @board = Board.new
    @current_player = player_one
    player_one.mark = :X
    player_two.mark = :O
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def switch_players!
    self.current_player = current_player == player_one ? player_two : player_one
  end

  def play
    current_player.display(board)

    while !board.over?
      play_turn
    end

    if game_winner
      game_winner.display(board)
      puts "#{game_winner.name} wins!"
    else
      puts "its a tie!"
    end
  end
  
  def game_winner
    return player_one if board.winner == player_one.mark
    return player_two if board.winner == player_two.mark
    nil
  end

if __FILE__ == $PROGRAM_NAME
  puts "Enter Name"
  name = gets.chomp
  human_player = HumanPlayer.new(name)
  computer_player = ComputerPlayer.new("computer")
  new_game = Game.new(human_player, computer_player)
  new_game.play
end

end
