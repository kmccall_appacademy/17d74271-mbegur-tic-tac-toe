class Board
  attr_reader :grid
  def initialize(grid=[[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    return true if self[pos] == nil
    false
  end

  def winner
    return :X if (columns + diagonal + grid).include?([:X, :X, :X])
    return :O if (columns + diagonal + grid).include?([:O, :O, :O])
    nil
  end

  def columns
    columns = Array.new(3) { [] }
    grid.each do |row|
      row.each_with_index do |mark, col_idx|
        columns[col_idx] << mark
      end
    end
    columns
    # first = [grid[0, 0], grid[1, 0], grid[2, 0]]
    # second = [grid[0, 1], grid[1, 1], grid[2, 1]]
    # third = [grid[0, 2], grid[1, 2], grid[2, 2]]
  end

  def diagonal
    # first = [grid[0, 0], grid[1, 1], grid[2, 2]]
    # second = [grid[0, 2], grid[1, 1], grid[2, 0]]
    # [first, second]
    first = [[0, 0], [1, 1], [2, 2]]
    second = [[0, 2], [1, 1], [2, 0]]

    [first, second].map do |diag|
      diag.map { |row, col| grid[row][col] }
    end
  end

  def over?
    return true if !grid.flatten.include?(nil)
    return true if winner
    false
  end

end
